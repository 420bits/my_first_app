# https://wsvincent.com/official-django-rest-framework-tutorial-beginners-guide/
from django.shortcuts import render
from django.http import HttpResponse
from rest_framework import generics
from .models import Question
from .serializers import SnippetSerializer
import os
def index(request):
    my_string: str = "My String Value 9"
    return HttpResponse(my_string)

class SnippetList(generics.ListCreateAPIView):
    queryset = Question.objects.all()
    serializer_class = SnippetSerializer


class SnippetDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Question.objects.all()
    serializer_class = SnippetSerializer
